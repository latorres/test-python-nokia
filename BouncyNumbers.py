# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 15:17:59 2019

@author: Luz Amparo Torres
"""

class BouncyNumbers:
    
    def __init__(self, proportion_bouncy_numbers):
       
        self.list_bouncy_number = [] #  List of bouncy numbers
        self.max_number = 2000000 # Maximum  number
        self.proportion_bouncy_numbers = proportion_bouncy_numbers #    Proportion of bouncy numbers 
    
    ## Find the least number for which the proportion of bouncy numbers is exactly proportion_bouncy_numbers
    def findNumber(self):             
        
        for number in range(0, self.max_number):
            number_str= str(number)
            increasing_number_flag = 0
            decreasing_number_flag = 0 
            if len(number_str) > 2 :

                for i in range(0, len(number_str) -1):
                    if int(number_str[i + 1]) < int(number_str[i]):
                        decreasing_number_flag = 1                    
                    if int(number_str[i + 1]) > int(number_str[i]):
                        increasing_number_flag = 1
               
                if increasing_number_flag == 1 and decreasing_number_flag == 1:
#                    is a bouncy number
                    self.list_bouncy_number.append(number_str)


                total_bouncy = len(self.list_bouncy_number)
                propotion = BouncyNumbers.ProportionBouncyNumber(self, total_bouncy, int(number))
            
                if propotion == self.proportion_bouncy_numbers:
                    print("The least number for which the proportion  of bouncy numbers is exactly " + str(propotion) + "%  is: " + str(number))
                    break
                
    def ProportionBouncyNumber(self, total_bouncy, total):
      
        proportion = (total_bouncy)/float(total) *100
       
        return proportion
                
                
                
obj = BouncyNumbers( 99.0)
obj.findNumber()